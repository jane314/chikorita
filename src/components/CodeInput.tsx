import { forwardRef } from "react";
import "./CodeInput.css";

const CodeInput = forwardRef((props, ref) => {
  return (
    <textarea
      ref={ref}
      value={props.inputValue}
      onChange={props.handleInput}
      onFocus={props.handleFocus}
      onKeyPress={props.handleKeydown}
    >
    </textarea>
  );
});

export { CodeInput };
